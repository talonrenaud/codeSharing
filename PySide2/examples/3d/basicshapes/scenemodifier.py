from PySide2.QtGui import QColor, QQuaternion, QVector3D
from PySide2.Qt3DCore import Qt3DCore
from PySide2.Qt3DExtras import Qt3DExtras


class SceneModifier(Qt3DCore.QEntity):
    def __init__(self, m_rootEntity, parent=None):
        super(SceneModifier, self).__init__()

        # Torus shape data
        self.m_torus = Qt3DExtras.QTorusMesh()
        self.m_torus.setRadius(1.0)
        self.m_torus.setMinorRadius(0.4)
        self.m_torus.setRings(100)
        self.m_torus.setSlices(20)

        # TorusMesh Transform
        self.torusTransform = Qt3DCore.QTransform()
        self.torusTransform.setScale(2.0)
        self.torusTransform.setRotation(QQuaternion.fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), 25.0))
        self.torusTransform.setTranslation(QVector3D(5.0, 4.0, 0.0))

        self.torusMaterial = Qt3DExtras.QPhongMaterial()
        self.torusMaterial.setDiffuse(QColor(50, 100, 20))

        self.m_torusEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_torusEntity.addComponent(self.m_torus)
        self.m_torusEntity.addComponent(self.torusMaterial)
        self.m_torusEntity.addComponent(self.torusTransform)

        #  Cone shape data
        self.cone = Qt3DExtras.QConeMesh()
        self.cone.setTopRadius(0.5)
        self.cone.setBottomRadius(1)
        self.cone.setLength(3)
        self.cone.setRings(50)
        self.cone.setSlices(20)

        # ConeMesh Transform
        self.coneTransform = Qt3DCore.QTransform()
        self.coneTransform.setScale(1.5)
        self.coneTransform.setRotation(QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), 45.0))
        self.coneTransform.setTranslation(QVector3D(0.0, 4.0, -1.5))

        self.coneMaterial = Qt3DExtras.QPhongMaterial()
        self.coneMaterial.setDiffuse(QColor(20, 100, 80))

        #  Cone
        self.m_coneEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_coneEntity.addComponent(self.cone)
        self.m_coneEntity.addComponent(self.coneMaterial)
        self.m_coneEntity.addComponent(self.coneTransform)

        #  Cylinder shape data
        self.cylinder = Qt3DExtras.QCylinderMesh()
        self.cylinder.setRadius(1)
        self.cylinder.setLength(3)
        self.cylinder.setRings(100)
        self.cylinder.setSlices(20)

        # CylinderMesh Transform
        self.cylinderTransform = Qt3DCore.QTransform()
        self.cylinderTransform.setScale(1.5)
        self.cylinderTransform.setRotation(QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), 45.0))
        self.cylinderTransform.setTranslation(QVector3D(-5.0, 4.0, -1.5))

        self.cylinderMaterial = Qt3DExtras.QPhongMaterial()
        self.cylinderMaterial.setDiffuse(QColor(80, 80, 120))

        #  Cylinder
        self.m_cylinderEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_cylinderEntity.addComponent(self.cylinder)
        self.m_cylinderEntity.addComponent(self.cylinderMaterial)
        self.m_cylinderEntity.addComponent(self.cylinderTransform)

        #  Cuboid shape data
        self.cuboid = Qt3DExtras.QCuboidMesh()

        #  CuboidMesh Transform
        self.cuboidTransform = Qt3DCore.QTransform()
        self.cuboidTransform.setScale(4.0)
        self.cuboidTransform.setTranslation(QVector3D(5.0, -4.0, 0.0))

        self.cuboidMaterial = Qt3DExtras.QPhongMaterial()
        self.cuboidMaterial.setDiffuse(QColor(45, 45, 140))

        # Cuboid
        self.m_cuboidEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_cuboidEntity.addComponent(self.cuboid)
        self.m_cuboidEntity.addComponent(self.cuboidMaterial)
        self.m_cuboidEntity.addComponent(self.cuboidTransform)

        # Plane shape data
        self.planeMesh = Qt3DExtras.QPlaneMesh()
        self.planeMesh.setWidth(2)
        self.planeMesh.setHeight(2)

        # Plane mesh transform
        self.planeTransform = Qt3DCore.QTransform()
        self.planeTransform.setScale(1.3)
        self.planeTransform.setRotation(QQuaternion.fromAxisAndAngle(QVector3D(1.0, 0.0, 0.0), 45.0))
        self.planeTransform.setTranslation(QVector3D(0.0, -4.0, 0.0))

        self.planeMaterial = Qt3DExtras.QPhongMaterial()
        self.planeMaterial.setDiffuse(QColor(50, 0, 120))

        # Plane
        self.m_planeEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_planeEntity.addComponent(self.planeMesh)
        self.m_planeEntity.addComponent(self.planeMaterial)
        self.m_planeEntity.addComponent(self.planeTransform)

        # Sphere shape data
        self.sphereMesh = Qt3DExtras.QSphereMesh()
        self.sphereMesh.setRings(20)
        self.sphereMesh.setSlices(20)
        self.sphereMesh.setRadius(2)

        # Sphere mesh transform
        self.sphereTransform = Qt3DCore.QTransform()
        self.sphereTransform.setScale(1.3)
        self.sphereTransform.setTranslation(QVector3D(-5.0, -4.0, 0.0))

        self.sphereMaterial = Qt3DExtras.QPhongMaterial()
        self.sphereMaterial.setDiffuse(QColor(0, 50, 100))

        # Sphere
        self.m_sphereEntity = Qt3DCore.QEntity(m_rootEntity)
        self.m_sphereEntity.addComponent(self.sphereMesh)
        self.m_sphereEntity.addComponent(self.sphereMaterial)
        self.m_sphereEntity.addComponent(self.sphereTransform)

    def enableTorus(self, enabled):
        self.m_torusEntity.setEnabled(enabled)

    def enableCone(self, enabled):
        self.m_coneEntity.setEnabled(enabled)

    def enableCylinder(self, enabled):
        self.m_cylinderEntity.setEnabled(enabled)

    def enableCuboid(self, enabled):
        self.m_cuboidEntity.setEnabled(enabled)

    def enablePlane(self, enabled):
        self.m_planeEntity.setEnabled(enabled)

    def enableSphere(self, enabled):
        self.m_sphereEntity.setEnabled(enabled)
