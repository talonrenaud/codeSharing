import sys

from PySide2.QtCore import QSize, Qt
from PySide2.QtGui import QColor, QVector3D
from PySide2.QtWidgets import QWidget, QCheckBox, QCommandLinkButton,\
    QHBoxLayout, QVBoxLayout, QApplication, QMainWindow
from PySide2.Qt3DCore import Qt3DCore
from PySide2.Qt3DRender import Qt3DRender
from PySide2.Qt3DExtras import Qt3DExtras
from PySide2.Qt3DInput import Qt3DInput

from scenemodifier import SceneModifier


class BasicShapes(QMainWindow):
    def __init__(self, parent=None):
        super(BasicShapes, self).__init__()

        # Root entity
        self.rootEntity = Qt3DCore.QEntity()

        # Set root object of the scene
        self.view = Qt3DExtras.Qt3DWindow()
        self.view.setRootEntity(self.rootEntity)

        self.view.defaultFrameGraph().setClearColor(QColor(0, 0, 0))
        self.container = QWidget.createWindowContainer(self.view)
        self.screenSize = self.view.screen().size()
        self.container.setMinimumSize(QSize(200, 100))
        self.container.setMaximumSize(self.screenSize)

        self.widget = QWidget()
        self.hLayout = QHBoxLayout(self.widget)
        self.vLayout = QVBoxLayout()
        self.vLayout.setAlignment(Qt.AlignTop)
        self.hLayout.addWidget(self.container, 1)
        self.hLayout.addLayout(self.vLayout)

        self.setWindowTitle("Basic shapes")

        self.viewInput = Qt3DInput.QInputAspect()
        self.view.registerAspect(self.viewInput)

        # Camera
        self.cameraEntity = self.view.camera()
        self.cameraEntity.lens().setPerspectiveProjection(45.0, 16.0 / 9.0, 0.1, 1000.0)
        self.cameraEntity.setPosition(QVector3D(0, 0, 20.0))
        self.cameraEntity.setUpVector(QVector3D(0, 1, 0))
        self.cameraEntity.setViewCenter(QVector3D(0, 0, 0))

        # Light
        self.lightEntity = Qt3DCore.QEntity(self.rootEntity)
        self.light = Qt3DRender.QPointLight(self.lightEntity)
        self.light.setColor("white")
        self.light.setIntensity(1)
        self.lightEntity.addComponent(self.light)
        self.lightTransform = Qt3DCore.QTransform(self.lightEntity)
        self.lightTransform.setTranslation(self.cameraEntity.position())
        self.lightEntity.addComponent(self.lightTransform)

        # For camera controls
        self.camController = Qt3DExtras.QFirstPersonCameraController(self.rootEntity)
        self.camController.setCamera(self.cameraEntity)

        # Scene modifier
        self.modifier = SceneModifier(self.rootEntity)

        # Create control widgets
        info = QCommandLinkButton()
        info.setText("Qt3D ready-made meshes")
        info.setDescription("Qt3D provides several ready-made meshes,\
            like torus, cylinder, cone, ""cube, plane and sphere.")
        info.setIconSize(QSize(0, 0))

        torusCB = QCheckBox(self.widget)
        torusCB.setChecked(True)
        torusCB.setText("Torus")

        coneCB = QCheckBox(self.widget)
        coneCB.setChecked(True)
        coneCB.setText("Cone")

        cylinderCB = QCheckBox(self.widget)
        cylinderCB.setChecked(True)
        cylinderCB.setText("Cylinder")

        cuboidCB = QCheckBox(self.widget)
        cuboidCB.setChecked(True)
        cuboidCB.setText("Cuboid")

        planeCB = QCheckBox(self.widget)
        planeCB.setChecked(True)
        planeCB.setText("Plane")

        sphereCB = QCheckBox(self.widget)
        sphereCB.setChecked(True)
        sphereCB.setText("Sphere")

        self.vLayout.addWidget(info)
        self.vLayout.addWidget(torusCB)
        self.vLayout.addWidget(coneCB)
        self.vLayout.addWidget(cylinderCB)
        self.vLayout.addWidget(cuboidCB)
        self.vLayout.addWidget(planeCB)
        self.vLayout.addWidget(sphereCB)

        torusCB.stateChanged.connect(self.modifier.enableTorus)
        coneCB.stateChanged.connect(self.modifier.enableCone)
        cylinderCB.stateChanged.connect(self.modifier.enableCylinder)
        cuboidCB.stateChanged.connect(self.modifier.enableCuboid)
        planeCB.stateChanged.connect(self.modifier.enablePlane)
        sphereCB.stateChanged.connect(self.modifier.enableSphere)

        torusCB.setChecked(True)
        coneCB.setChecked(True)
        cylinderCB.setChecked(True)
        cuboidCB.setChecked(True)
        planeCB.setChecked(True)
        sphereCB.setChecked(True)

        self.setCentralWidget(self.widget)


if __name__ == '__main__':
    try:
        QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
        QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps)
    except Exception as e:
        pass

    app = QApplication(sys.argv)

    mainWin = BasicShapes()
    mainWin.resize(1200, 800)
    mainWin.show()

    sys.exit(app.exec_())
