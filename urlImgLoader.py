import sys

if (sys.version_info > (3, 0)):
	from urllib.request import urlopen
else:
	import io
	from urllib2 import urlopen

from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *

class UrlImgLoaderSignals(QObject):
	if (sys.version_info > (3, 0)):
		receivedData = Signal(bytes)
	else:
		receivedData = Signal(io.BytesIO)	# use BytesIO obj for Python 2

class UrlImgLoaderRunnable(QRunnable):
	def __init__(self, url, parent = None ):
		QRunnable.__init__(self , parent)
		
		self.url = url
		self.signals = UrlImgLoaderSignals()

	def run(self):
		imageData = urlopen(self.url).read()
		
		if (sys.version_info < (3, 0)):			# use BytesIO obj for Python 2
			imageData = io.BytesIO(imageData)

		self.signals.receivedData.emit(imageData)

class MainUI(QDialog):
	def __init__(self, parent = None ):
		QDialog.__init__(self , parent)
		
		self.setWindowTitle("UrlImgLoader - test")
		self.mainLyt = QGridLayout()
		
		# Create Img Loader Thread Pool
		self.imgLoaderThreadPool = QThreadPool()
		self.imgLoaderThreadPool.setMaxThreadCount(10)
				
		# Create UI Widgets
		self.urlField = QLineEdit("https://i.vimeocdn.com/video/629880984.jpg?mw=1920&mh=1080&q=70")
		self.urlField.setToolTip("Press enter to validate URL")
		self.imgViewer = QLabel()
		
		# Set Layout
		self.mainLyt.addWidget(self.urlField,	0, 0)
		self.mainLyt.addWidget(self.imgViewer,	1, 0)
		
		# Create connections
		self.urlField.returnPressed.connect(self.loadImgFromThread)
		
		# Set Dialog's layout
		self.setLayout(self.mainLyt)
		
		# Load deafault image on startup
		self.loadImgFromThread()
		
	def loadImgFromThread(self):
		imgLoadWorker = UrlImgLoaderRunnable(self.urlField.text())
		imgLoadWorker.signals.receivedData.connect(self.displayreceivedImg)

		self.imgLoaderThreadPool.start(imgLoadWorker)
	
	def displayreceivedImg(self, imgData):
		
		if (sys.version_info > (3, 0)):
			img = QImage.fromData(imgData)	# Python 3
		else:
			img = QImage.fromData(imgData.getvalue()) # Python 2
			
		pixmap = QPixmap.fromImage(img)
		self.imgViewer.setPixmap(pixmap)
	
if __name__ == '__main__':
	app = QApplication(sys.argv)

	mainWin = MainUI()
	mainWin.resize(800,600)
	mainWin.show()
	mainWin.activateWindow()
	mainWin.raise_()
	
	mainWin.show()
	sys.exit(app.exec_())
